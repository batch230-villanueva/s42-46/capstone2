const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers.js");
const auth = require("../auth.js");

router.get("/order", auth.verify, orderControllers.order);

router.get("/getAllOrders", orderControllers.getOrders);

router.get("/getOrders", orderControllers.getUserOrders);

module.exports = router;
