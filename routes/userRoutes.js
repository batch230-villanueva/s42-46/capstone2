const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");

router.post("/signup", userControllers.signUp);

router.post("/checkEmail", userControllers.checkEmails);

router.post("/login", userControllers.login);

router.get("/getUsers", userControllers.getAllUsers);

router.get("/getUser", auth.verify, userControllers.getUserInfo);

router.patch("/setAdmin/:userID", userControllers.setAdmin);

router.patch("/removeAdmin/:userID", userControllers.removeAdmin);

router.put("/addToCart", userControllers.addToUserCart);

router.patch("/updateCartItem", userControllers.updateCartQuantity);

router.patch("/removeCartItem", userControllers.removeFromCart);

router.post("/getItemSubtotal", userControllers.getItemSubtotal);

router.get("/getCartTotal", userControllers.getCartTotal);

router.get("/getUserName/:userId", userControllers.getUserName);

module.exports = router;
