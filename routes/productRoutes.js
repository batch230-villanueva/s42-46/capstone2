const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js");
const auth = require("../auth.js");

router.post("/addProduct",auth.verify , productControllers.addProduct);

router.get("/getActiveProducts", productControllers.getActiveProducts);

router.get("/getAllProducts", productControllers.getAllProducts);

router.get("/:productId", productControllers.getProductInfo);

router.patch("/updateInfo/:prodId", productControllers.updateProductInfo);

router.patch("/archive/:prodId", productControllers.archiveProduct);

router.patch("/unarchive/:prodId", productControllers.unarchiveProduct);

router.get("/getProductName/:prodId", productControllers.getProductName);

router.get("/getProductPrice/:prodId", productControllers.getProductPrice);

module.exports = router;