/*
	Gitbash:
	npm init -y
	npm install express
	npm install mongoose
	npm install cors
*/

// Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");
const orderRoutes = require("./routes/orderRoutes.js");

// to create a express server/application
const app = express();

// Middlewares - allows to bridge our backend application (server) to our front end
// to allow cross origin resourse sharing
app.use(cors());
// to read json objects (especially request that is in form of json)
app.use(express.json());
// to read forms
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

// Connect to our MongoDB database
mongoose.connect("mongodb+srv://admin:admin@batch230.cqzdm3c.mongodb.net/villanuevaCapstone?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once("open", () => console.log("Now connected to Villanueva-Mongo DB Atlas"));

app.listen(process.env.PORT || 4000, () => 
	{ console.log(`API is now online on port ${process.env.PORT || 4000} `)
});
