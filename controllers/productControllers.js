const mongoose = require("mongoose");
const Product = require("../models/product.js");
const auth = require("../auth.js");

module.exports.addProduct = (request, response) => {

    const authData = auth.decode(request.headers.authorization)

    if (authData.isAdmin == true) {
        let newProduct = new Product(
            {
                name: request.body.name,
                description: request.body.description,
                price: request.body.price,
				stocks: request.body.stocks
            }
        )
    
        return newProduct.save().then((newProduct, error) => {
            if(error){
                return response.send(false);
            } else {
                return response.send(newProduct);
            }
        })
    } else {
        let message = Promise.resolve("ERROR: This is an ADMIN-ONLY feature!");
        return message.then((value) => {response.send(value)});
    }
}

module.exports.getActiveProducts = (request, response) => {
    return Product.find({isActive:true}).then(result => response.send(result)).catch(error => response.send(false));
}

module.exports.getAllProducts = (request, response) => {

    const authData = auth.decode(request.headers.authorization)

    if (authData.isAdmin == true) {
        return Product.find({}).then(result => response.send(result)).catch(error => response.send(false));
    } else {
        let message = Promise.resolve("ERROR: This is an ADMIN-ONLY feature!");
        return message.then((value) => {response.send(value)});
    }
}

module.exports.getProductInfo = (request, response) => {
    return Product.findById(request.params.productId).then(result => response.send(result)).catch(error => response.send(false));
}

// module.exports.updateProductInfo = (prodId, newData) => {
//     if (newData.isAdmin === true) {
//         return Product.findByIdAndUpdate(prodId, {
//             name: newData.product.name,
//             description: newData.product.description,
//             price: newData.product.price,
//         })
//         .then((res, err) => {
//             if (err) return false;
//             return res;
//         })
//     } else {
//         let message = Promise.resolve("ERROR: This is an ADMIN-ONLY feature!");

//         return message.then(value => {
//             return value;
//         })
//     }
// }

module.exports.updateProductInfo = (request, response) => {

    const authData = auth.decode(request.headers.authorization);
    const prodId = request.params.prodId;
    const product = request.body;

    if (authData.isAdmin === true) {
        return Product.findByIdAndUpdate(prodId, {
            name: product.name,
            description: product.description,
            price: product.price,
            stocks: product.stocks
        })
        .then((result, error) => {
            if (error) response.send(error);
            response.send(result);
        })
    } else {
        let message = Promise.resolve("ERROR: This is an ADMIN-ONLY feature!");

        return message.then(value => {
            response.send(value);
        })
    }
}

// module.exports.archiveProduct = (prodId, authData) => {
// 	if(authData.isAdmin == true){
// 		return Product.findByIdAndUpdate(prodId,
// 			{
// 				isActive: false
// 			}
// 		).then((result, error)=>{
// 			if(error){
// 				return false;
// 			}
// 			// return true
//             return `Item "${result.name}" is now archived.`
// 		})
// 	}
// 	else{
// 		let message = Promise.resolve('ERROR: This is an ADMIN-ONLY feature!');
// 		return message.then((value) => {return value});
// 	}
// }

module.exports.archiveProduct = (request, response) => {
    const authData = auth.decode(request.headers.authorization);

	if(authData.isAdmin){
		return Product.findByIdAndUpdate(request.params.prodId,
			{
				isActive: false
			}
		).then((result, error)=>{
			if(error){
				response.send(error);
			}
			// return true
            response.send(`Item "${result.name}" is now archived.`);
		})
	}
	else{
		let message = Promise.resolve('ERROR: This is an ADMIN-ONLY feature!');
		return message.then((value) => response.send(value));
	}
}

module.exports.unarchiveProduct = (request, response) => {
    const authData = auth.decode(request.headers.authorization);

	if(authData.isAdmin){
		return Product.findByIdAndUpdate(request.params.prodId,
			{
				isActive: true
			}
		).then((result, error)=>{
			if(error){
				response.send(error);
			}
			// return true
            response.send(`Item "${result.name}" is now active.`);
		})
	}
	else{
		let message = Promise.resolve('ERROR: This is an ADMIN-ONLY feature!');
		return message.then((value) => response.send(value));
	}
}

module.exports.getProductName = (request,response) => {
    return Product.findById(request.params.prodId).then(
        result => response.send({
            "productName" : result.name
        })
    ).catch(err => response.send(404))
}

module.exports.getProductPrice = (request,response) => {
    return Product.findById(request.params.prodId).then(
        result => response.send({
            "productPrice" : result.price
        })
    ).catch(err => response.send(404))
}