const User = require("../models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Order = require("../models/order.js");
const Product = require("../models/product.js");

module.exports.signUp = async (request, response) => {
    let newUser = new User({
        firstName: request.body.firstName,
        lastName: request.body.lastName,
        email: request.body.email,
        password: bcrypt.hashSync(request.body.password, 10),
        mobileNo: request.body.mobileNo
    })

    return await newUser.save().then((user,err) => {
        if (err) response.send(err);

        response.send(user);
    }
    )
}

module.exports.login = (request, response) => {
    return User.findOne({email: request.body.email}).then(
        result => {
            if (result == null) {response.send(false)}
            
            else {
                const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

                if(isPasswordCorrect) {
                    response.send({access: auth.createAccessToken(result)});
                } else {
                    response.send(false);
                }
            } 

            
        }
    )
}

// CHECKING
module.exports.getAllUsers = (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    if (userData.isAdmin){
        return User.find({}).then(result => response.send(result));
    } else {
        return response.send(403);
    }

    
}

module.exports.getUserInfo = (request, response) => {

    const userData = auth.decode(request.headers.authorization);

	console.log(userData);

	return User.findById(userData.id).then(result =>{
		result.password = "***";
		response.send(result);
	}).catch(err => response.send(err));

	// return User.findById(request.body._id).then((result, err)=> {
	// 	if(err) response.send(err);
		
	// 	result.password = "";
	// 	response.send(result);
	// })
}

module.exports.checkEmails = (request, response) =>{
	return User.find({email: request.body.email}).then(result =>{
		console.log(result);

		if(result.length > 0){
			return response.send(true);
		}
		else{
			return	response.send(false);
		}
	})
	.catch(error => response.send(error));
}


module.exports.setAdmin = async (request, response) => {
    const adminUser = auth.decode(request.headers.authorization);

    console.log(adminUser)

    if(adminUser.isAdmin) {

        await User.findById(request.params.userId)
        .then(
            user => {
                user.isAdmin = true;
                return user.save();
            }
        ).catch(
            err => {
                return response.send(err);
            }
        )

        response.send(true);
    } else {
        let message = Promise.resolve("ERROR: This is an ADMIN-ONLY feature!");

        return message.then(value => {
            response.send(value);
        })
    }
}

module.exports.removeAdmin = async (request, response) => {
    const adminUser = auth.decode(request.headers.authorization);

    console.log(adminUser)

    if(adminUser.isAdmin) {

        await User.findById(request.params.userId)
        .then(
            user => {
                user.isAdmin = false;
                return user.save();
            }
        ).catch(
            err => {
                return response.send(err);
            }
        )

        response.send(true);
    } else {
        let message = Promise.resolve("ERROR: This is an ADMIN-ONLY feature!");

        return message.then(value => {
            response.send(value);
        })
    }
}

module.exports.addToUserCart = async (request, response) => {
    const user = auth.decode(request.headers.authorization);

    const {prodId, quantity} = request.body;

    if (!user.isadmin){

        console.log(user);

        await User.findById(user.id)
        .then(async user => {
            
            let product = await Product.findById(prodId).then(result => result).catch(error => {
                console.log(404);
            });
    
            console.log("PRODUCT")
            console.log(prodId)
            console.log(product);

            if (!product || !product.isActive || !product.stocks || product.stocks - quantity <= 0){
                return response.send(false);
            } else {

                const index = user.cart.map(item => item.productId).indexOf(product.id);

                if (index === -1) {
                    console.log('new')

                    user.cart.push({
                        productId: prodId,
                        quantity: quantity,
                        subTotal: product.price * quantity
                    });
                    
                } else {
                    console.log('add')

                    user.cart[index].quantity += quantity;
                    user.cart[index].subTotal = product.price * quantity;
                }

                user.save();

                return response.send(user);
            }

        }).catch(error => {
            console.log("HEY")
            console.log(error);
            return response.send(error);
        })

    } else {
        let message = Promise.resolve("ERROR: This is an CUSTOMER-ONLY feature!");

        return message.then(value => {
            response.send(value);
        })
    }
}

module.exports.updateCartQuantity = async (request, response) => {
    const user = auth.decode(request.headers.authorization);

    const {prodId, quantity} = request.body;

    if (!user.isadmin){

        console.log(user);

        await User.findById(user.id)
        .then(async user => {
            
            if (user.cart.length === 0) return response.send('Cart is empty') 

            let product = await Product.findById(prodId).then(result => result).catch(error => {
                console.log(404);
            });

            if (!product || !product.isActive || !product.stocks || product.stocks - quantity <= 0){
                return response.send(false);
            } else {

                const index = user.cart.map(item => item.productId).indexOf(product.id);

                if (index === -1) {
                    return response.send('Item not in cart');
                } else {
                    user.cart[index].quantity = quantity;
                    user.cart[index].subTotal = product.price * quantity;
                }

                user.save();

                return response.send(user);
            }

        }).catch(error => {
            return response.send(error);
        })

    } else {
        let message = Promise.resolve("ERROR: This is an CUSTOMER-ONLY feature!");

        return message.then(value => {
            response.send(value);
        })
    }
}

module.exports.removeFromCart = async (request, response) => {
    const user = auth.decode(request.headers.authorization);

    const {prodId} = request.body;

    if (!user.isadmin){

        console.log(user);

        await User.findById(user.id)
        .then(async user => {
            
            if (user.cart.length === 0) return response.send(false) 

            let product = await Product.findById(prodId).then(result => result).catch(error => {
                console.log(404);
            });

            if (!product || !product.isActive){
                return response.send(false);
            } else {

                const index = user.cart.map(item => item.productId).indexOf(product.id);

                if (index === -1) {
                    return response.send('Item not in cart');
                } else {
                    user.cart.splice(index, 1);
                }

                user.save();

                return response.send(user);
            }

        }).catch(error => {
            return response.send(error);
        })

    } else {
        let message = Promise.resolve("ERROR: This is an CUSTOMER-ONLY feature!");

        return message.then(value => {
            response.send(value);
        })
    }
}

module.exports.getItemSubtotal = async (request, response) => {
    const user = auth.decode(request.headers.authorization);

    const {prodId} = request.body;

    if (!user.isadmin){

        await User.findById(user.id)
        .then(async user => {
            
            if (user.cart.length === 0) return response.send('Cart is empty') 

            let product = await Product.findById(prodId).then(result => {
                result}).catch(error => {
                console.log(404);
            });

            console.log(product)

            if (!product || !product.isActive){
                return response.send(false);
            } else {

                const index = user.cart.map(item => item.productId).indexOf(product.id);

                if (index === -1) {
                    return response.send('Item not in cart');
                } else {
                    return response.send({"item_subtotal" : user.cart[index].subTotal});
                }
            }

        }).catch(error => {
            return response.send(error);
        })

    } else {
        let message = Promise.resolve("ERROR: This is an CUSTOMER-ONLY feature!");

        return message.then(value => {
            response.send(value);
        })
    }
}

module.exports.getCartTotal = async (request, response) => {
    const user = auth.decode(request.headers.authorization);

    if (!user.isadmin){

        console.log(user);

        await User.findById(user.id)
        .then(async user => {
            
            if (user.cart.length === 0) return response.send('Cart is empty') 

            let totalPrice = 0;

            user.cart.forEach(
                item => 
                totalPrice += item.subTotal
            )
            response.send({"cart_total" : totalPrice});

        }).catch(error => {
            console.log(error)
            return response.send(error);
        })

    } else {
        let message = Promise.resolve("ERROR: This is an CUSTOMER-ONLY feature!");

        return message.then(value => {
            response.send(value);
        })
    }
}

module.exports.getUserName = (request,response) => {
    return User.findById(request.params.userId).then(
        result => response.send(
            {
                "name" : `${result.firstName} ${result.lastName}`
            }
        )
    ).catch(err => response.send(404))
}