const mongoose = require("mongoose");
const Order = require("../models/order.js");
const Product = require("../models/product");
const User = require("../models/user");
const auth = require("../auth.js");

// module.exports.order = async (request, response) => {
//     const userData = auth.decode(request.headers.authorization)

//     const {prodId, quantity} = request.body;

//     if (!userData.isAdmin) {
        
//         let product = await Product.findById(prodId).then(result => result).catch(error => {
//             console.log(404);
//         });

//         if (!product || !product.isActive || !product.stocks || product.stocks - quantity <= 0){
//             response.send(false);
            
//         } else {
//             let orderData = {
//                 totalAmount: quantity * product.price,
//                 userId: userData.id,
//                 products: [
//                     {
//                         productId: prodId,
//                         quantity: quantity,
//                     },
//                 ]
//             }
    
//             let newOrder = new Order(orderData);

//             await newOrder.save()
//             .then(result => {
//                 console.log(result);
//                 return true;
//             }).catch(err => {
//                 console.log(err);
//                 return false;
//             })

//             await Product.findById(prodId)
//             .then(prod => {
//                 prod.stocks -= quantity;

//                 return prod.save()
//                 .then(result => {
//                     console.log(result)
//                     return true;
//                 })
//                 .catch(error => {
//                     console.log(error);
//                     return false;
//                 })
//             })

//             response.send(newOrder);
//         }

//     } else {
//         let message = Promise.resolve("ERROR: This is a CUSTOMER-ONLY feature!");

//         return message.then(value => {
//             response.send(value);
//         })
//     }
// }

module.exports.order = async (request, response) => {
    const userData = auth.decode(request.headers.authorization)

    if (!userData.isAdmin) {
        
        console.log('userData')
        console.log(userData)

        let orderData = {
            totalAmount: 0,
            userId: userData.id,
            products: []
        }

        await User.findById(userData.id).then(async user => {
            const {cart} = user

            console.log('cart')
            console.log(cart)

            if (cart && cart.length !== 0) {

                for (let i = 0; i < cart.length; i++) {
                    let product = await Product.findById(cart[i].productId).then(result => result).catch(error => {
                        console.log(404);
                    });
    
                    if (!product || !product.isActive || !product.stocks || product.stocks - cart[i].quantity <= 0){
                        return response.send(false);
                    } else {
                        orderData.products.push(
                            {
                                productId: cart[i].productId,
                                quantity: cart[i].quantity
                            }
                        )
    
                        orderData.totalAmount += product.price * cart[i].quantity;
    
                        product.stocks -= cart[i].quantity;
                        product.save();

                        

                    }
                }

                console.log('heyhey')

                while(cart.length !== 0) {
                    user.cart.pop()
                }

                user.save();
    
                let newOrder = new Order(orderData);
    
                await newOrder.save()
                .then(result => {
                    console.log(result);
                    return true;
                }).catch(err => {
                    console.log(err);
                    return response.send(err);
                })
    
                response.send(newOrder);
            } else {
                response.send(false);
            }
        })

    } else {
        let message = Promise.resolve("ERROR: This is a CUSTOMER-ONLY feature!");

        return message.then(value => {
            response.send(value);
        })
    }
}

module.exports.getOrders = (request, response) => {
    const isAdmin = auth.decode(request.headers.authorization).isAdmin;

    if(isAdmin) {
        Order.find({}).then(result => response.send(result));
    } else {
        let message = Promise.resolve("ERROR: This is an ADMIN-ONLY feature!");

        return message.then(value => {
            response.send(value);
        })
    }
}

module.exports.getUserOrders = async (request, response) => {
    const user = auth.decode(request.headers.authorization);

    if(user.isAdmin) {return response.send("ERROR: This is a CUSTOMER-ONLY feature!")}

   await Order.find({userId : user.id})
   .then(result => response.send(result))
   .catch(error => response.send(error))
}