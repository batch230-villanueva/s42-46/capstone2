// dependency
const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema(
	{
		totalAmount: {
			type: Number,
			required: [true, "Order Amount is required"]
		},

		createdOn : {
			type: Date,
			default: new Date()
		},

		userId : {
			type: String,
			required: [true, "User ID is required"]
		},

		products : [
            {
				_id: false,

                productId : {
                    type : String,
                    required : [true, "Product ID is required"]
                },

                quantity : {
                    type : Number,
                    required : [true, "Product Quantity is required"]
                }
            }
        ]
		
	}
) 

module.exports = mongoose.model("Order", orderSchema);