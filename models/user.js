// dependency
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
	{
		firstName: {
			type: String,
			required: [true, "First Name is required"]
		},

		lastName: {
			type: String,
			required: [true, "Last Name is required"]
		},

		email : {
			type: String,
			required: [true, "Email is required"]
		},

		password: {
			type: String,
			required: [true, "Passowrd is required"]
		},

        mobileNo: {
			type: String,
			required: [true, "Mobile Number is required"]
		},

		isAdmin:{
			type: Boolean,
			default: false
		},

		cart: [
			{
				_id:false,
				productId: {
					type: String,
					required: [true, "Product ID is required"]
				},
				quantity: {
					type: Number,
					required: [true, "Quantity is required"]
				},
				subTotal: {
					type: Number,
					required: [true, "SubTotal is required"]
				},
				
			}
		]
	}
) 

module.exports = mongoose.model("User", userSchema);


